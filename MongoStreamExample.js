const MongoClient = require('mongodb').MongoClient;

class MongoStreamExample {

  async run() {
    console.log('Connecting...');
    const user = 'eosReader';
    const password = encodeURIComponent('Replace with password');
    try {
      this.client = await MongoClient.connect(`mongodb://${user}:${password}@ec2-52-50-58-42.eu-west-1.compute.amazonaws.com:27017/EOS`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      console.log('Connected.');
      this.eosDb = this.client.db('EOS');
      const changeStream = this.eosDb.watch();
      console.log('Listening to change events...');
      changeStream.on('change', next => {
        console.log(next);
      });

    } catch (err) {
      console.error(err);
    } finally {
      //this.client && this.client.close();
      //console.log('Connection closed.');
    }
  }

}

new MongoStreamExample().run();