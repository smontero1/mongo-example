const MongoClient = require('mongodb').MongoClient;

class MongoExample {

  async run() {
    console.log('Connecting...');
    const user = 'eosReader';
    const password = encodeURIComponent('Replace with password');
    const account = 'seedsuseraaa';
    try {
      this.client = await MongoClient.connect(`mongodb://${user}:${password}@ec2-52-50-58-42.eu-west-1.compute.amazonaws.com:27017/EOS`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      console.log('Connected.');
      this.eosDb = this.client.db('EOS');
      await this.allResultsAsArrayExample(account);
      await this.allResultsIteratorExample(account);
      await this.paginateResultsExample(account, 2, 3);
    } catch (err) {
      console.error(err);
    } finally {
      this.client && this.client.close();
      console.log('Connection closed.');
    }
  }

  async getTransferActions(account) {
    return this.eosDb.collection('action_traces').find({
      "act.account": "seedstokennx",
      $or: [{ "act.data.from": account }, { "act.data.to": account }]
    });
  }

  async allResultsAsArrayExample(account) {
    const items = await this.getTransferActions(account);
    console.log('All items using toArray:', JSON.stringify(await items.toArray(), null, 2));
  }

  async allResultsIteratorExample(account) {
    const items = await this.getTransferActions(account);
    console.log('All items using each:');
    return new Promise((resolve, reject) => {
      items.each((err, item) => {
        if (item) {
          console.log(item);
        } else {
          resolve();
        }
      });
    });
  }

  async paginateResultsExample(account, skip, limit) {
    const items = await this.getTransferActions(account);
    items.skip(skip).limit(limit);
    console.log('Specific items by setting skip and limit:', JSON.stringify(await items.toArray(), null, 2));
  }
}

new MongoExample().run();