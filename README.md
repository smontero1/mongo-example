Example showing how to query the mongodb eos database, because find method returns a cursor object there are many ways to access the data, in this example 3 are shown:

1.  Access all items as array
2.  Access all items by iterating cursor
3.  Access specific items by specifying a skip and limit

Run Example:

1.  Set password of eosReader in [MongoExample.js](https://gitlab.com/smontero1/mongo-example/blob/master/MongoExample.js#L8)
2.  Run: `npm install`
3.  Run example: `node MongoExample.js`